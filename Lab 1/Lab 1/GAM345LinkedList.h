#pragma once

namespace GAM345
{
	template <class Type>
	class LinkedList
	{
	public:
		void push_back(Type& aValue)
		{
			if (mFirst == nullptr)
			{
				mFirst = new LinkedListElement(aValue);
				mSize++;
				return;
			}
			LinkedListElement *temp = mFirst;
			while (temp->next != nullptr)
			{
				temp = temp->next;
			}
			temp->next = new LinkedListElement(aValue);
			mSize++;
		}

		Type& operator[](int aIndex)
		{
			aIndex = aIndex % mSize; // Bad index
			if (aIndex == 0)
				return mFirst->value;
			
			LinkedListElement *temp = mFirst;
			while (temp->next != nullptr)
			{
				temp = temp->next;
				aIndex--;
				if (aIndex <= 0)
				{
					break;
				}
			}
			return temp->value;
		}

	private:
		struct LinkedListElement
		{
			LinkedListElement(Type& aValue) :value(aValue), next(nullptr) {}
			Type value;
			LinkedListElement *next;
		};

		int mSize;
		LinkedListElement *mFirst;
	};
}
