#pragma once
namespace GAM345
{
	typedef char byte;

	template <class Type>
	class Vector
	{
	public:
		Vector() : mSize(0), mCapacity(0), mData(nullptr)
		{

		}

		void push_back(Type aElement) {
			if (mCapacity < mSize + 1) {
				reserve(mSize * 2);
			}
			mData[mSize] = aElement;
			mSize++;
		}

		void reserve(int aNewCapacity) {
			Type* temp = new byte[sizeof(Type) * aNewCapacity];
			memcpy(temp, mData, sizeof(Type) * mSize);
			delete[] mData;
			mData = temp;
			mCapacity = aNewCapacity;
		}

		void resize(int aSize) {
			if (aSize > mSize)
			{
				reserve(aSize);
			}
			for (int i = mSize; i < aSize; i++)
			{
				(mData + i)->Type();
			}
			mSize = aSize;
		}

		void insert(Type& input, int index)
		{
			if (mCapacity < mSize + 1) {
				reserve(mSize * 2);
			}
			
			for (int i = mSize; i > index; i--)
			{
				*(mData + i) = *(mData + i - 1);
			}
			*(mData + index) = input;
			mSize++;

			memmove(mData + index + 1, mData + index, sizeof(Type) * (mSize - index));
		}

		int size()
		{
			return mSize;
		}

		Type& operator[](int aIndex)
		{
			return *(mData + aIndex);
		}

		bool empty()
		{
			return mSize == 0;
		}

		int capacity()
		{
			return mCapacity;
		}

		bool remove(int aIndex)
		{
			// 8
			// remove 3
			// 0 1 2 4 5 6 7
			//*(mData + aIndex).~Type();
			//memmove(mData + aIndex, mData + index + 1, sizeof(Type) * (mSize - index));
			//mSize--;
			return true;
		}

	private:
		
		int mSize;
		int mCapacity;
		Type *mData;
	};
}