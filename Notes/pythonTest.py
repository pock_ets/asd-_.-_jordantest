
def Test(input):
    print(input)

a = 5
Test(a)

b = "Hello"
Test(b)

c = b + str(a)
Test(c)

if c == "Hello5":
    print("C is Hello5!")
    c = "Hi" 
    print("We are here")
else:
    print("C is not Hello5, it is " + c)
