#include <cstdlib>
#include <iostream>

bool findElement(int value, int *a, int size, int &index)
{
	for (int i = 0; i < size; i++)
	{
		if (a[i] == value)
		{
			index = i;
			return true;
		}
	}
	return false;
}

bool findElementSorted(
	int value, int *a, int size, int &index)
{
	for (int i = 0; i < size; i++)
	{
		if (a[i] > value)
			break;

		if (a[i] == value)
		{
			index = i;
			return true;
		}
	}
	return false;
}

int main(int argc, char** argv)
{
	int a[1000];

	for (int i = 0; i < 1000; i++)
	{
		//a[i] = rand() % 100;
		a[i] = i + 200;
	}

	int index = -1;
	if (findElement(56, a, 1000, index))
	{
		std::cout << "Element found at index " << index << std::endl;
	}
	else
	{
		std::cout << "Element missing" << std::endl;
	}

}