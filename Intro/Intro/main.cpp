#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <Windows.h>
#include "TemplateExample.h"

void PrintCurrentTime(std::string &desc, unsigned long long aStartTime)
{
	std::cout << desc.c_str() << " took " << GetTickCount64() - aStartTime << std::endl;
}

int main(int argc, char** argv)
{
	unsigned long long startTime; //GetTickCount64();
	unsigned long long endTime;
	const int dataSize = 100;
	std::string name;

	startTime = GetTickCount64();
	std::vector<int> stdVector;
	for (int i = 0; i < dataSize; i++)
	{
		stdVector.insert(stdVector.end(), i);
	}
	name = "Vector";
	PrintCurrentTime(name, startTime);

	startTime = GetTickCount64();
	std::list<int> stdList;
	for (int i = 0; i < dataSize; i++)
	{
		stdList.insert(stdList.begin(), i);
	}
	name = "List";
	PrintCurrentTime(name, startTime);

	
	
	int *p = new int;
	*p = 5; // 00000000 00000000 00000000 00000101
	std::cout << p << ": " << (*p) << std::endl;
	delete p;

	p = new int;
	*p = 10;
	std::cout << p << ": " << (*p) << std::endl;
	delete p;

	
	p = new int[5];
	*(p) = 0;
	*(p + 1) = 1;
	p[2] = 2;
	p[3] = 3;
	*(p + 4) = 4;

	std::cout << p[1] << std::endl;

	delete[] p;


	TemplateExample<int> exampleA;
	TemplateExample<int> exampleB;
	TemplateExample<int> exampleC;

	//exampleA = exampleB + exampleC;
	exampleA = exampleB;

	std::string first("Test");
	std::string second("Hello");
	first = second;
	first.operator=(second);
	

	return 0;
}
