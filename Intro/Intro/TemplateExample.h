#pragma once

template <class Type>
class TemplateExample
{
public:
	TemplateExample() {}

	Type GetData()
	{
		return data;
	}

	void SetData(Type aNewValue)
	{
		data = aNewValue;
	}

	TemplateExample& operator=(const TemplateExample& other)
	{

	}

private:
	Type data;
};