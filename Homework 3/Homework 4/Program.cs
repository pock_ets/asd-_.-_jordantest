﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_4
{class Program{

static void Main(string[] args) {
            //This creates a new array and stores 5 variables within it
            //This will allow us to later us while loops to call upon certain values
            int[] Counts;
            ;
            Counts[0] = 5;
            Counts[1] = 10;
            Counts[2] = 15;
            Counts[3] = 20;
            Counts[4] = 25;

            //This loop runs 5 times, and each time it runs, prints out the value in that specific point in the array
            int countUp = 0;
            while (countUp < 5)
            {
                Console.WriteLine("Index " + countUp + " has a value of " + Counts[countUp]);
                countUp++;
            }

            Console.WriteLine("");

            //This loop runs 5 times, and each time it encounters an even number it increases its value by one
            //This loop will only print out and change even numbers
            int countUp2 = 0;
            while (countUp2 < 5)
            {
                if (Counts[countUp2] % 2 == 0)
                {
                    //This changes the original value of the even slots to make them permentantly odd
                    Counts[countUp2] += 1;
                    Console.WriteLine("Index " + countUp2 + " was " + (Counts[countUp2]-1) + " and is now being changed to " + (Counts[countUp2]));

                }
                countUp2++;
            }

            Console.WriteLine("");

            //This creates a list similar to the first, but now with the newly changed numbers
            //This loop runs 5 times and prints out all the values in the array
            int countUp3 = 0;
            while (countUp3 < 5)
            {
                Console.WriteLine("Index " + countUp3 + " has a value of " + Counts[countUp3]);
                countUp3++;
            }

        }
    }
}


