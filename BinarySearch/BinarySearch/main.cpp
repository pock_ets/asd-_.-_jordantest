#include <iostream>

int binarySearch(int* aArray, int aSize, int aValue)
{
	return -1;
}

int binarySearchRecursive(int* aArray, int aSize, int aValue)
{
	// Compare the value against the middle index
	int midIndex = aSize / 2
	// If mid == value, return index (done)
	if (aArray[midIndex] == aValue)
		return midIndex;
	else if (midIndex == 0)
		return -1;

	// If mid < value, call BSearch(upper)
	if (aArray[midIndex] < aValue)
	{
		return binarySearchRecursive((aArray + midIndex), midIndex, aValue);
	}

	// If mid > value, call BSearch(lower)
	if (aArray[midIndex] > aValue)
	{
		return binarySearchRecursive(aArray, midIndex, aValue);
	}
}

int main(int argc, char** argv)
{
	int a[] = { 1, 3, 6, 8, 9, 10, 23, 45 };

	//int index = binarySearch(a, 8, 1);
	//std::cout << "Value found at index: " << index << std::endl;

	int index = binarySearchRecursive(a, 8, 1);
	std::cout << "Value found at index: " << index << std::endl;

	return 0;
}

