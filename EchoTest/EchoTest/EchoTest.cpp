// EchoTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>

int main()
{
	std::string input;
	std::cin >> input;
    std::cout << input;
	return 0;
}
